//
//  AddNodeViewDelegate.h
//  test
//
//  Created by IOS developer on 16.08.16.
//  Copyright © 2016 myorg. All rights reserved.
//

@import UIKit;

@protocol nodeOperationsDelegate <NSObject>

@optional

-(void) addNode:(NSString*)value :(NSString*)parentID;
-(void) deleteNode:(Node*)nodeToDelete;
-(void) changeNodeValue:(Node*)nodeToChange :(NSString*)newValue;

@end