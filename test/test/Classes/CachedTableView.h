//
//  ClientTableViewController.h
//  test
//
//  Created by IOS developer on 13.08.16.
//  Copyright © 2016 myorg. All rights reserved.
//

@import UIKit;

@interface CachedTableView : UITableView <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray* nodesArray;

-(void) getSortedArrayForTableFromArray:(NSArray*)nodes;

@end
