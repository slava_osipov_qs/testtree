//
//  Node.m
//  test
//
//  Created by IOS developer on 12.08.16.
//  Copyright © 2016 myorg. All rights reserved.
//

#import "Node.h"

@implementation Node

-(id) init
{
	self = [super init];
	if( !self ) return nil;
	
	self.uid = [[[NSUUID UUID] UUIDString] lowercaseString];
	
	return self;
}

-(id) initWithValueAndParentID:(NSString*)value :(NSString*)parentID
{
	self = [super init];
	if( !self )
		return nil;
	
	self.uid = [[[NSUUID UUID] UUIDString] lowercaseString];
	self.value = [NSString stringWithString:value];
	self.parentID = [[NSString stringWithString:parentID] lowercaseString];
	self.isDeleted = false;
	self.children = [[NSMutableArray alloc] init];
	self.level = 0;
	self.needToSync = false;
	self.syncType = 0;
	
	return self;
}

-(id) initWithDictionary:(NSDictionary *)node
{
	self = [super init];
	if( !self )
		return nil;
	
	self.uid = [[node valueForKey:@"id"] lowercaseString];
	self.value = [node valueForKey:@"value"];
	self.parentID = [[node valueForKey:@"parentID"] lowercaseString];
	self.isDeleted = [[node valueForKey:@"isDeleted"] boolValue];
	self.children = [[NSMutableArray alloc] init];
	self.level = 0;
	self.needToSync = false;
	self.syncType = 0;
	
	return self;
}

-(BOOL) addChild: (Node*) node
{
	node.parentID = [NSString stringWithString:self.uid];
	node.level = self.level + 1;
	[self.children addObject:node];
	
	if (self.isDeleted)
	{
		node.isDeleted = self.isDeleted;
	}
	
	return true;
}

-(BOOL) deleteNodeChilds
{
	if ([self.children count] > 0)
	{
		[self.children enumerateObjectsUsingBlock:^(Node*child, NSUInteger idx, BOOL *stop){			
			[child deleteNodeChilds];
			child.isDeleted = true;
		}];
	}
	
	return true;
}

-(BOOL) changeValue: (NSString*) newValue
{
	self.value = [NSString stringWithString:newValue];
	
	return true;
}

-(id)copyWithZone:(NSZone *)zone
{
	Node* newObj = [[Node alloc] init];
	
	newObj.uid = [NSString stringWithString:self.uid];
	newObj.value = [NSString stringWithString:self.value];
	newObj.isDeleted = self.isDeleted;
	newObj.level = self.level;
	newObj.syncType = self.syncType;
	newObj.needToSync = self.needToSync;
	newObj.parentID = [NSString stringWithString:self.parentID];
	newObj.children = [self.children mutableCopy];
	
	return self;
}

@end