//
//  Database.m
//  test
//
//  Created by IOS developer on 13.08.16.
//  Copyright © 2016 myorg. All rights reserved.
//

#import "Database.h"


@implementation Database

-(id) init
{
	self = [super init];
	if( !self ) return nil;
	
	self->DB = [self getInitialDB];
	
	return self;
}

-(NSArray*)getInitialDB
{
	NSLog(@"[Database]getInitialDB");
	
	return @[
				//root
				[NSMutableDictionary dictionaryWithDictionary: @{
					@"id": @"26157b13-74c1-4ecf-975e-ce0325360f53",
					@"value": @"root",
					@"isDeleted": @"false",
					@"parentID":@"",
					@"children":
							//1-level childs
							[NSMutableArray arrayWithObjects:
								[NSMutableDictionary dictionaryWithDictionary: @{
									@"id": @"b55e23f1-386e-44af-85cf-6e19a3f13759",
									@"value": @"Node1",
									@"isDeleted": @"false",
									@"parentID": @"26157b13-74c1-4ecf-975e-ce0325360f53",
									@"children":
											[NSMutableArray arrayWithObjects:
												[NSMutableDictionary dictionaryWithDictionary: @{
													@"id": @"4981297e-3c4a-4793-b9b9-ce0b5d48b15c",
													@"value": @"Node11",
													@"isDeleted": @"false",
													@"parentID": @"b55e23f1-386e-44af-85cf-6e19a3f13759",
													@"children": [[NSMutableArray alloc]init]
												}],
												
												[NSMutableDictionary dictionaryWithDictionary: @{
													@"id": @"94b336dd-7fd0-4c32-91ba-38d9d8549c9f",
													@"value": @"Node12",
													@"isDeleted": @"false",
													@"parentID": @"b55e23f1-386e-44af-85cf-6e19a3f13759",
													@"children":
														[NSMutableArray arrayWithObjects:
															[NSMutableDictionary dictionaryWithDictionary: @{
																@"id": @"4e05811c-0130-4fc0-b96a-62a4299b68b7",
																@"value": @"Node121",
																@"isDeleted": @"false",
																@"parentID": @"94b336dd-7fd0-4c32-91ba-38d9d8549c9f",
																@"children": [[NSMutableArray alloc] init]
															}],
															
															[NSMutableDictionary dictionaryWithDictionary: @{
																@"id": @"801faf56-aa98-4925-900b-37355806a5b7",
																@"value": @"Node122",
																@"isDeleted": @"false",
																@"parentID": @"94b336dd-7fd0-4c32-91ba-38d9d8549c9f",
																@"children":
																	[NSMutableArray arrayWithObjects:
																		[NSMutableDictionary dictionaryWithDictionary: @{
																			@"id": @"da5d5b1c-3f0b-461c-8d37-0976ad0da72a",
																			@"value": @"Node1221",
																			@"isDeleted": @"false",
																			@"parentID": @"801faf56-aa98-4925-900b-37355806a5b7",
																			@"children": [[NSMutableArray alloc] init]
																		}],
																	 
																		[NSMutableDictionary dictionaryWithDictionary: @{
																			@"id": @"4d3acf2f-2345-4cd7-8020-569176f383c7",
																			@"value": @"Node1222",
																			@"isDeleted": @"false",
																			@"parentID": @"801faf56-aa98-4925-900b-37355806a5b7",
																			@"children": [[NSMutableArray alloc] init]
																		}],nil
																	]
															}],nil
														]
												}],
												
												[NSMutableDictionary dictionaryWithDictionary: @{
													@"id": @"d20507ac-d491-40c0-b2da-82cc7696e59b",
													@"value": @"Node13",
													@"isDeleted": @"false",
													@"parentID": @"b55e23f1-386e-44af-85cf-6e19a3f13759",
                                                    @"children":
                                                        [NSMutableArray arrayWithObjects:
                                                         [NSMutableDictionary dictionaryWithDictionary: @{
                                                                                                          @"id": @"da5d5b1c-3f0b-4e1c-8d37-0976ad0da7ea",
                                                                                                          @"value": @"Node131",
                                                                                                          @"isDeleted": @"false",
                                                                                                          @"parentID": @"d20507ac-d491-40c0-b2da-82cc7696e59b",
                                                                                                          @"children": [[NSMutableArray alloc] init]
                                                                                                          }],
                                                         
                                                         [NSMutableDictionary dictionaryWithDictionary: @{
                                                                                                          @"id": @"4d3acf2f-23e5-4cd7-8020-569176f383e7",
                                                                                                          @"value": @"Node132",
                                                                                                          @"isDeleted": @"false",
                                                                                                          @"parentID": @"d20507ac-d491-40c0-b2da-82cc7696e59b",
                                                                                                          @"children": [[NSMutableArray alloc] init]
                                                                                                          }],nil
                                                         ]
												}],nil
											]
								}],
								
								[NSMutableDictionary dictionaryWithDictionary: @{
									@"id": @"94f421b6-c6d3-4fd5-a873-8ad42e0e1016",
									@"value": @"Node2",
									@"isDeleted": @"false",
									@"parentID": @"26157b13-74c1-4ecf-975e-ce0325360f53",
									@"children": [NSMutableArray arrayWithObjects:
													[NSMutableDictionary dictionaryWithDictionary: @{
													   @"id": @"679a67ec-157d-4bd7-be07-1e5aa0c10b9b",
													   @"value": @"Node21",
													   @"isDeleted": @"false",
													   @"parentID": @"94f421b6-c6d3-4fd5-a873-8ad42e0e1016",
													   @"children": [[NSMutableArray alloc] init]
													}],
												  
													[NSMutableDictionary dictionaryWithDictionary: @{
													   @"id": @"9eaa4ccb-1e9d-4cf1-84c6-fd58ac335c32",
													   @"value": @"Node22",
													   @"isDeleted": @"false",
													   @"parentID": @"94f421b6-c6d3-4fd5-a873-8ad42e0e1016",
                                                       @"children":
                                                           [NSMutableArray arrayWithObjects:
                                                            [NSMutableDictionary dictionaryWithDictionary: @{
                                                                                                             @"id": @"da5d5b1c-3a0b-461c-8d37-0976ad0da7aa",
                                                                                                             @"value": @"Node221",
                                                                                                             @"isDeleted": @"false",
                                                                                                             @"parentID": @"94f421b6-c6d3-4fd5-a873-8ad42e0e1016",
                                                                                                             @"children": [[NSMutableArray alloc] init]
                                                                                                             }],
                                                            
                                                            [NSMutableDictionary dictionaryWithDictionary: @{
                                                                                                             @"id": @"4d3acf2f-2345-4caa-8020-569176f383a7",
                                                                                                             @"value": @"Node222",
                                                                                                             @"isDeleted": @"false",
                                                                                                             @"parentID": @"94f421b6-c6d3-4fd5-a873-8ad42e0e1016",
                                                                                                             @"children": [[NSMutableArray alloc] init]
                                                                                                             }],nil
                                                            ]

													}],
												  
													[NSMutableDictionary dictionaryWithDictionary: @{
													   @"id": @"647d5720-35af-456e-8984-ae7c8212e5eb",
													   @"value": @"Node23",
													   @"isDeleted": @"false",
													   @"parentID": @"94f421b6-c6d3-4fd5-a873-8ad42e0e1016",
													   @"children": [[NSMutableArray alloc] init]
													}],nil
												  ]
								}],
								
								[NSMutableDictionary dictionaryWithDictionary: @{
									@"id": @"1537a2a4-1828-41fa-804f-ba05bde9117d",
									@"value": @"Node3",
									@"isDeleted": @"false",
									@"parentID": @"26157b13-74c1-4ecf-975e-ce0325360f53",
									@"children": [[NSMutableArray alloc] init]
								}],nil
							]
				}]
			];
}

-(NSArray*) getDataBase
{
	return self->DB;
}

-(NSArray*) getDataBaseForTable
{
	__block NSMutableArray* DBForTable = [[NSMutableArray alloc] init];
	__block NSInteger level = 0;
	
	__block __weak void (^nodesIntoArrayBlock_weak)(NSMutableDictionary*, NSUInteger, BOOL * _Nonnull);
	void (^nodesIntoArrayBlock)(NSMutableDictionary*, NSUInteger, BOOL * _Nonnull);
	
	nodesIntoArrayBlock_weak = nodesIntoArrayBlock = ^(NSMutableDictionary* obj, NSUInteger idx, BOOL * _Nonnull stop)
	{
		NSMutableDictionary* newobj = [NSMutableDictionary dictionaryWithDictionary:obj];
		
		[newobj setObject:[NSString stringWithFormat:@"%ld", (long)level] forKey:@"level"];
		[DBForTable addObject:newobj];
	
		if ([[obj valueForKey:@"children"] count] > 0)
		{
			level++;
			[[obj valueForKey:@"children"] enumerateObjectsUsingBlock:nodesIntoArrayBlock_weak];
			level--;
		}
	};
	
	[self->DB enumerateObjectsUsingBlock:nodesIntoArrayBlock];
	
	return DBForTable;
}

-(NSMutableDictionary*)getNodeByID:(NSString *)nodeID :(BOOL)copying
{
	NSLog(@"[Database]getNodeByID");
	__block NSMutableDictionary* foundedNode = nil;
	
	__block __weak void (^findNodeBlock_weak)(NSMutableDictionary*, NSUInteger, BOOL * _Nonnull);
	void (^findNodeBlock)(NSMutableDictionary*, NSUInteger, BOOL * _Nonnull);
	
	findNodeBlock_weak = findNodeBlock = ^(NSMutableDictionary* obj, NSUInteger idx, BOOL * _Nonnull stop)
	{
		if ([[obj valueForKey:@"id"] isEqualToString:nodeID])
		{
			foundedNode = obj;
		
			*stop = YES;
		}
		else
		{
			if ([[obj valueForKey:@"children"] count] > 0)
			{
				[[obj valueForKey:@"children"] enumerateObjectsUsingBlock:findNodeBlock_weak];
			}
		}
	};
	
	[self->DB enumerateObjectsUsingBlock:findNodeBlock];
	
	if (foundedNode && copying)
	{
		foundedNode = [NSMutableDictionary dictionaryWithDictionary:foundedNode];
		[foundedNode removeObjectForKey:@"children"];
	}
	
	if (foundedNode && [[foundedNode valueForKey:@"isDeleted"] boolValue])
	{
		return nil;
	}
	
	return foundedNode;
}

-(void) resetDatabase
{
	self->DB = [NSArray arrayWithArray:[self getInitialDB]];
}

-(NSInteger) getNodesCount
{
	__block int count = 0;
	
	__block __weak void (^nodesCountBlock_weak)(NSDictionary*, NSUInteger, BOOL * _Nonnull);
	void (^nodesCountBlock)(NSDictionary*, NSUInteger, BOOL * _Nonnull);
	
	nodesCountBlock_weak = nodesCountBlock = ^(NSDictionary* obj, NSUInteger idx, BOOL * _Nonnull stop)
	{
		
		if ([[obj valueForKey:@"children"] count] > 0)
		{
			[[obj valueForKey:@"children"] enumerateObjectsUsingBlock:nodesCountBlock_weak];
		}
		
		count++;
	};
	
	[self->DB enumerateObjectsUsingBlock:nodesCountBlock];
	
	return count;
}

#pragma mark Sync methods

-(void) syncAddChange:(NSArray *)array
{
	NSLog(@"[Database]syncAddChange");
	for (Node* obj in array)
	{
		NSMutableDictionary* foundedNode = [self getNodeByID:obj.uid :false];
	
		if (foundedNode)
		{
			[foundedNode setValue:obj.value forKey:@"value"];
			[foundedNode setValue:obj.parentID forKey:@"parentID"];
		}
		else
		{
			[[[self getNodeByID:obj.parentID :false] objectForKey:@"children"] addObject:[NSMutableDictionary dictionaryWithObjects:@[obj.uid,obj.value,obj.parentID,[[NSMutableArray alloc]init],[NSString stringWithFormat:@"%d", obj.isDeleted]] forKeys:@[@"id",@"value",@"parentID",@"children",@"isDeleted"]]];
		}
	
	
	}
}


-(NSArray*) syncDelete:(NSArray *)array
{
	NSLog(@"[Database]syncDelete");
	__block NSMutableArray *deletedNodes = [[NSMutableArray alloc] init];
	
	__block __weak void (^deleteRecursiveBlock_weak)(NSMutableDictionary*, NSUInteger, BOOL * _Nonnull);
	void (^deleteRecursiveBlock)(NSMutableDictionary*, NSUInteger, BOOL * _Nonnull);
	
	deleteRecursiveBlock_weak = deleteRecursiveBlock = ^(NSMutableDictionary* obj, NSUInteger idx, BOOL * _Nonnull stop)
		{
			[obj setValue:@"true" forKey:@"isDeleted"];
			[deletedNodes addObject:[obj valueForKey:@"id"]];
		
			if ([[obj objectForKey:@"children"] count] > 0)
			{
				[[obj objectForKey:@"children"] enumerateObjectsUsingBlock:deleteRecursiveBlock_weak];
			}
		};
	
	for (Node* obj in array)
	{
		NSMutableDictionary* foundedNode = [self getNodeByID:obj.uid :false];
	
		[foundedNode setValue:@"true" forKey:@"isDeleted"];
		[deletedNodes addObject:[foundedNode valueForKey:@"id"]];
	
		[(NSMutableArray*)[foundedNode objectForKey:@"children"] enumerateObjectsUsingBlock:deleteRecursiveBlock];
	}
	
	return deletedNodes;
}

@end