//
//  AddNodeView.h
//  test
//
//  Created by IOS developer on 13.08.16.
//  Copyright © 2016 myorg. All rights reserved.
//

@import UIKit;

#import "Node.h"
#import "nodeOperationsDelegate.h"

@interface AddNodeViewController : UIViewController

@property IBOutlet UILabel* infoLabel;
@property IBOutlet UITextField* textFieldValue;
@property NSString* parentID;

@property (nonatomic, weak) id<nodeOperationsDelegate> delegate;

-(IBAction) addBtnClick;
-(IBAction) cancel;

@end