//
//  DatabaseTableViewCell.h
//  test
//
//  Created by IOS developer on 15.08.16.
//  Copyright © 2016 myorg. All rights reserved.
//

@import UIKit;

@interface DatabaseTableViewCell : UITableViewCell

@property IBOutlet UILabel* valueLabel;
@property IBOutlet NSLayoutConstraint* labelValueLeading;
@property IBOutlet UIView* valueLabelView;

@end
