//
//  ChangeNodeViewController.m
//  test
//
//  Created by IOS developer on 14.08.16.
//  Copyright © 2016 myorg. All rights reserved.
//

#import "ChangeNodeViewController.h"

@implementation ChangeNodeViewController

-(IBAction) changeBtnClick
{
	if ([self.textFieldValue.text length])
	{
		[self.delegate changeNodeValue:self.nodeToChangeValue :self.textFieldValue.text];
		[self dismissViewControllerAnimated:true completion:nil];
	}
	else
	{
		UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Adding node",nil) message:NSLocalizedString(@"Empty value is not allowed",nil) preferredStyle:UIAlertControllerStyleAlert];
		
		UIAlertAction* alertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok",nil) style:UIAlertActionStyleDefault handler:nil];
		[alert addAction:alertAction];
		
		[self presentViewController:alert animated:true completion:nil];
		
		return;
	}
}

-(IBAction) cancel
{
	[self dismissViewControllerAnimated:true completion:nil];
}

@end