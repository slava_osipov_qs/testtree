//
//  Database.h
//  test
//
//  Created by IOS developer on 13.08.16.
//  Copyright © 2016 myorg. All rights reserved.
//

#import "Tree.h"
#import "Node.h"

@interface Database : NSObject
{
	NSArray* DB;
}

-(NSArray*) getDataBase;
-(NSArray*) getDataBaseForTable;
-(NSInteger) getNodesCount;
-(NSMutableDictionary*) getNodeByID:(NSString*)nodeID :(BOOL)copying;

-(void) syncAddChange:(NSArray*) array;
-(NSArray*) syncDelete:(NSArray*) array;

-(void) resetDatabase;

@end
