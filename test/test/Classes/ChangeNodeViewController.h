//
//  ChangeNodeViewController.h
//  test
//
//  Created by IOS developer on 13.08.16.
//  Copyright © 2016 myorg. All rights reserved.
//

@import UIKit;

#import "Node.h"
#import "nodeOperationsDelegate.h"

@interface ChangeNodeViewController : UIViewController

@property IBOutlet UILabel* infoLabel;
@property IBOutlet UITextField* textFieldValue;

@property Node* nodeToChangeValue;

@property (nonatomic, weak) id<nodeOperationsDelegate> delegate;

-(IBAction) changeBtnClick;
-(IBAction) cancel;

@end