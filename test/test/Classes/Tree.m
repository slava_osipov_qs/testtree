//
//  Tree.m
//  test
//
//  Created by IOS developer on 12.08.16.
//  Copyright © 2016 myorg. All rights reserved.
//

#import "Tree.h"

@implementation Tree

-(Tree*) initWithRoot: (Node*) node
{
	Tree* tree = [[Tree alloc] init];
	
	tree.root = node;
	
	return tree;
}

@end
