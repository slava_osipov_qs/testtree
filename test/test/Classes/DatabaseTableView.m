//
//  DBTableViewController.m
//  test
//
//  Created by IOS developer on 13.08.16.
//  Copyright © 2016 myorg. All rights reserved.
//

#import "DatabaseTableView.h"
#import "DatabaseTableViewCell.h"

const int k_spacingWidth = 10;

@implementation DatabaseTableView


#pragma mark UITableViewDataSource methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.nodesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	DatabaseTableViewCell *cell = nil;
	
	cell = (DatabaseTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"DatabaseTableViewCell"];	
		cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
	
	
	NSDictionary* node = self.nodesArray[[indexPath row]];
	NSString* value = [node valueForKey:@"value"];
		
	cell.textLabel.text = value;
	
	if ([[node valueForKey:@"isDeleted"] boolValue])
	{
		cell.textLabel.textColor = [UIColor redColor];
	}
	else
	{
		cell.textLabel.textColor = [UIColor blackColor];
	}
	
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
	{
		[cell.textLabel setFont:[UIFont systemFontOfSize:10]];
	}
	
	return cell;
}

- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSInteger row = [indexPath row];
	
	if(row < [self.nodesArray count])
	{
		NSInteger level = [[[self.nodesArray objectAtIndex:[indexPath row]] valueForKey:@"level"] intValue];
	
		return level;
	}
	
	return 0;
}

@end