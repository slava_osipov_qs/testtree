//
//  Node.h
//  test
//
//  Created by IOS developer on 12.08.16.
//  Copyright © 2016 myorg. All rights reserved.
//

@import Foundation;

@interface Node : NSObject <NSCopying>

@property NSString* uid;
@property (nonatomic, strong) NSString *value;
@property BOOL isDeleted;
@property NSInteger level;

//0 - add or change, 1 - delete
@property int syncType;
@property BOOL needToSync;

@property (nonatomic, strong) NSString *parentID;
@property (nonatomic, strong) NSMutableArray *children;

-(id) init;
-(id) initWithDictionary:(NSDictionary*)node;
-(id) initWithValueAndParentID:(NSString*)value :(NSString*)parentID;

-(BOOL) addChild: (Node*) node;
-(BOOL) deleteNodeChilds;
-(BOOL) changeValue: (NSString*) newValue;

@end