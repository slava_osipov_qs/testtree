//
//  Header.h
//  test
//
//  Created by IOS developer on 12.08.16.
//  Copyright © 2016 myorg. All rights reserved.
//

#import "Node.h"

@interface Tree : NSObject

@property (nonatomic, strong) Node *root;

-(id) initWithRoot: (Node*) node;

@end