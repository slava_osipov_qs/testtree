//
//  CachedTableView.m
//  test
//
//  Created by IOS developer on 14.08.16.
//  Copyright © 2016 myorg. All rights reserved.
//

#import "CachedTableView.h"
#import "CachedTableViewCell.h"
#import "Node.h"

@implementation CachedTableView

#pragma mark UITableViewDataSource methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.nodesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CachedTableViewCell *cell = nil;
	
	cell = (CachedTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"CachedTableViewCell"];
	cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
	
	
	Node* node = self.nodesArray[[indexPath row]];
	NSString* value = node.value;
	
	cell.textLabel.text = value;
	
	if ([[node valueForKey:@"isDeleted"] boolValue])
	{
		cell.textLabel.textColor = [UIColor redColor];
	}
	else
	{
		cell.textLabel.textColor = [UIColor blackColor];
	}
	
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
	{
		[cell.textLabel setFont:[UIFont systemFontOfSize:10]];
	}
	
	return cell;
}

- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSInteger row = [indexPath row];
	NSInteger _level = 0;
	
	if(row < [self.nodesArray count])
	{
		Node* node = [self.nodesArray objectAtIndex:[indexPath row]];
	
		if (node && [node respondsToSelector:@selector(level)])
		{
			_level = node.level;
		}
	}
	
	return _level;
}

- (void) getSortedArrayForTableFromArray:(NSArray*)nodes
{
	__block NSMutableArray* sortedArrayForTable = [[NSMutableArray alloc] init];
	__block NSInteger level = 0;
	
	__block __weak void (^sortArrayBlock_weak)(Node*, NSUInteger, BOOL * _Nonnull);
	void (^sortArrayBlock)(Node*, NSUInteger, BOOL * _Nonnull);
	
	sortArrayBlock_weak = sortArrayBlock = ^(Node* obj, NSUInteger idx, BOOL * _Nonnull stop)
	{
		obj.level = (long)level;
		[sortedArrayForTable addObject:obj];
		
		if ([[obj valueForKey:@"children"] count] > 0)
		{
			level++;
			[[obj valueForKey:@"children"] enumerateObjectsUsingBlock:sortArrayBlock_weak];
			level--;
		}
	};
	
	[nodes enumerateObjectsUsingBlock:sortArrayBlock];
	
	self.nodesArray = sortedArrayForTable;
}


@end