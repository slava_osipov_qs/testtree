//
//  ServerTableViewController.h
//  test
//
//  Created by IOS developer on 13.08.16.
//  Copyright © 2016 myorg. All rights reserved.
//

@import UIKit;
#import "Database.h"

@interface DatabaseTableView : UITableView <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray* nodesArray;

@end
