//
//  AddNodeViewController.m
//  test
//
//  Created by IOS developer on 14.08.16.
//  Copyright © 2016 myorg. All rights reserved.
//

#import "AddNodeViewController.h"

@implementation AddNodeViewController

-(IBAction) addBtnClick
{
	if ([self.textFieldValue.text length])
	{
		[self.delegate addNode:self.textFieldValue.text :self.parentID];
		[self dismissViewControllerAnimated:true completion:nil];
	}
	else
	{
		UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Adding node" message:@"Empty value is not allowed" preferredStyle:UIAlertControllerStyleAlert];
		
		UIAlertAction* alertAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
		[alert addAction:alertAction];
		
		[self presentViewController:alert animated:true completion:nil];
		
		return;
	}
}

-(IBAction) cancel
{
	[self dismissViewControllerAnimated:true completion:nil];
}

@end