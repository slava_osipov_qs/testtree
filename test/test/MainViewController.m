//
//  ViewController.m
//  test
//
//  Created by IOS developer on 12.08.16.
//  Copyright © 2016 myorg. All rights reserved.
//

#import "MainViewController.h"
#import "AddNodeViewController.h"
#import "ChangeNodeViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewWillAppear:(BOOL)animated
{
	
}

- (void)viewDidLoad {
	[super viewDidLoad];
	
	self.database = [[Database alloc] init];
	self.databaseTBView.nodesArray = [self.database getDataBaseForTable];
	
	self.cachedArray = [[NSMutableArray alloc] init];
	self.cachedTBView.nodesArray = nil;
	
	self.databaseTBView.dataSource = self.databaseTBView;
	self.databaseTBView.delegate = self.databaseTBView;
	
	self.cachedTBView.dataSource = self.cachedTBView;
	self.cachedTBView.delegate = self.cachedTBView;
	
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
	{
		self.cachedTBView.rowHeight = 25;
		self.databaseTBView.rowHeight = 25;
	}
	else
	{
		self.cachedTBView.rowHeight = 40;
		self.databaseTBView.rowHeight = 40;
	}
	
		// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark IBActions

-(IBAction) loadNodeBtnClick
{
	NSString* nodeID = [[self.databaseTBView.nodesArray objectAtIndex:[ [self.databaseTBView indexPathForSelectedRow] row] ] valueForKey:@"id"];
	
	NSInteger nodeIdx = [self.cachedTBView.nodesArray indexOfObjectPassingTest: ^BOOL(Node *obj, NSUInteger idx, BOOL *stop) {
		BOOL found = [obj.uid isEqualToString:nodeID];
		return found;
	}];
	
	if (nodeIdx != NSNotFound && [self.cachedTBView.nodesArray count])
	{
		UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Loading node",nil) message:NSLocalizedString(@"This node was already downloaded",nil) preferredStyle:UIAlertControllerStyleAlert];
	
		UIAlertAction* alertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok",nil) style:UIAlertActionStyleDefault handler:nil];
		[alert addAction:alertAction];
	
		[self presentViewController:alert animated:true completion:nil];
	
		return;
	}
	
	NSDictionary* loadedNode = [self.database getNodeByID:nodeID :true];
	
	if (!loadedNode)
	{
		UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Loading node",nil) message:NSLocalizedString(@"Node is not found",nil) preferredStyle:UIAlertControllerStyleAlert];
		
		UIAlertAction* alertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok",nil) style:UIAlertActionStyleDefault handler:nil];
		[alert addAction:alertAction];
		
		[self presentViewController:alert animated:true completion:nil];

		return;
	}
	
	if (!self.cachedArray)
	{
		self.cachedArray = [[NSMutableArray alloc] init];
		self.cachedTBView.nodesArray = nil;
	}
	
	//	NSSortDescriptor *levelDescriptor = [[NSSortDescriptor alloc] initWithKey:@"level" ascending:YES];
	//	NSArray *sortDescriptors = [NSArray arrayWithObject:levelDescriptor];
	//	self.cachedArray = [NSMutableArray arrayWithArray:[self.cachedArray sortedArrayUsingDescriptors:sortDescriptors]];
	
	__block Node* newNode = [[Node alloc] initWithDictionary:loadedNode];
	__block BOOL added = false;
	
	__block NSMutableArray* temp_cachedArray = [self.cachedArray mutableCopy];
	
		//search parent or childs on zero-level
	for (Node *obj in self.cachedArray)
	{
		if ([newNode.parentID isEqualToString:obj.uid])
		{
			[obj addChild:newNode];
			added = true;
		}
		else if ([obj.parentID isEqualToString:newNode.uid])
		{
			[newNode addChild:obj];
			[temp_cachedArray removeObject:obj];
		
			if (obj.isDeleted && [obj.children count] > 0)
				[obj deleteNodeChilds];
		}
	}
	
		//search parent on the leaf
	if (!added)
	{
		
		__block __weak void (^goDownBlock_weak)(Node*, NSUInteger, BOOL * _Nonnull);
		void (^goDownBlock)(Node*, NSUInteger, BOOL * _Nonnull);
	
		__block Node* foundedParentNode = nil;
		
		goDownBlock_weak = goDownBlock = ^(Node* obj, NSUInteger idx, BOOL * _Nonnull stop)
		{
			if (added) *stop = true;
		
			if ([newNode.parentID isEqualToString:obj.uid])
			{
				foundedParentNode = obj;
				__block MainViewController* mvc = self;
			
				[[mvc findNodeWithIDRecursively:foundedParentNode.uid:temp_cachedArray] addChild:newNode];
			
				added = true;
			}
			else
			{
				if ([obj.children count] > 0)
				{
					[obj.children enumerateObjectsUsingBlock:goDownBlock_weak];
				}
			}
		
		};
	
		[self.cachedArray enumerateObjectsUsingBlock:goDownBlock];
	
		if (!added)
		{
			[temp_cachedArray addObject:newNode];
		}
	}
	
	self.cachedArray = temp_cachedArray;
	
	[self refreshCacheTable];
}

	//sync cache with database
-(IBAction) applyChangesBtnClick
{
	__block NSInteger syncType;
	
	BOOL (^searchSyncNeeded)(Node*, NSUInteger, BOOL * _Nonnull) = ^BOOL(Node *obj, NSUInteger idx, BOOL *stop) {
		return obj.needToSync && (obj.syncType & syncType);
	};
	
	syncType = syncTypeAdd | syncTypeChange;
	NSIndexSet* indexesSyncAddChange = [self.cachedTBView.nodesArray indexesOfObjectsPassingTest:searchSyncNeeded];
	NSArray* cacheToSyncAddChange = [self.cachedTBView.nodesArray objectsAtIndexes:indexesSyncAddChange];
	
	syncType = syncTypeDelete;
	NSIndexSet *indexesSyncDelete = [self.cachedTBView.nodesArray indexesOfObjectsPassingTest:searchSyncNeeded];
	NSArray* cacheToSyncDelete = [self.cachedTBView.nodesArray objectsAtIndexes:indexesSyncDelete];
	
	NSSortDescriptor *levelDescriptor = [[NSSortDescriptor alloc] initWithKey:@"level" ascending:YES];
	NSArray *sortDescriptors = [NSArray arrayWithObject:levelDescriptor];
	
	cacheToSyncAddChange = [NSArray arrayWithArray:[cacheToSyncAddChange sortedArrayUsingDescriptors:sortDescriptors]];
	cacheToSyncDelete = [NSArray arrayWithArray:[cacheToSyncDelete sortedArrayUsingDescriptors:sortDescriptors]];
	
	if ([cacheToSyncAddChange count] == 0 && [cacheToSyncDelete count] == 0)
	{
		UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Sync data",nil) message:NSLocalizedString(@"There is nothing to sync",nil) preferredStyle:UIAlertControllerStyleAlert];
		
		UIAlertAction* alertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok",nil) style:UIAlertActionStyleDefault handler:nil];
		[alert addAction:alertAction];
		
		[self presentViewController:alert animated:true completion:nil];
		return;
	}
	
		//sync call
	[self.database syncAddChange:cacheToSyncAddChange];
	NSArray *responseDeletedIDs = [self.database syncDelete:cacheToSyncDelete];
	
	for (__block NSString* deletedNodeID in responseDeletedIDs)
	{
		BOOL (^searchDeletedNodes)(Node*, NSUInteger, BOOL * _Nonnull) = ^BOOL(Node *obj, NSUInteger idx, BOOL *stop) {
			return [obj.uid isEqualToString: deletedNodeID];
		};
	
		NSUInteger foundedIdx = [self.cachedArray indexOfObjectPassingTest:searchDeletedNodes];
	
		if (foundedIdx != NSNotFound)
		{
			Node* deletedNode = [self.cachedArray objectAtIndex:foundedIdx];
		
			deletedNode.isDeleted = true;
			[deletedNode deleteNodeChilds];
		}
	}
	
	[[cacheToSyncDelete arrayByAddingObjectsFromArray:cacheToSyncAddChange] enumerateObjectsUsingBlock:^(Node* obj, NSUInteger idx, BOOL * _Nonnull stop){
		obj.needToSync = false;
		obj.syncType = 0;
	}];
	
	[[self cachedTBView]reloadData];
	[self refreshDatabaseTable];
}

	//reset app to the initial state
-(IBAction) resetAllBtnClick
{
	self.cachedArray = [[NSMutableArray alloc] init];
	self.cachedTBView.nodesArray = [[NSMutableArray alloc] init];
	
	[[self database] resetDatabase];
	
	[self refreshDatabaseTable];
	[self refreshCacheTable];
}

	//add new child node to the selected one
-(IBAction) addNodeBtnClick
{
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"AddNodeViewController" bundle:nil];
	
	AddNodeViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"AddNodeViewController"];
	
	if ([self.cachedArray count] == 0)
	{
		UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Adding node",nil) message:NSLocalizedString(@"Download any nodes please before adding new one",nil) preferredStyle:UIAlertControllerStyleAlert];
		
		UIAlertAction* alertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok",nil) style:UIAlertActionStyleDefault handler:nil];
		[alert addAction:alertAction];
		
		[self presentViewController:alert animated:true completion:nil];
		
		return;
	}
	
	NSInteger selectedIndex = [[self.cachedTBView indexPathForSelectedRow] row];
	Node* selectedNode = self.cachedTBView.nodesArray[selectedIndex];
	
	if (selectedNode.isDeleted)
	{
		[self showAlertNodeIsDeleted];
		return;
	}
	
	controller.parentID = selectedNode.uid;
	
	controller.delegate = self;
	controller.modalPresentationStyle = UIModalPresentationPopover;
	controller.preferredContentSize = CGSizeMake(400, 130);

	UIPopoverPresentationController *presentationController = [controller popoverPresentationController];
	
		//move popover arrow to the vertical center of "add" button
	presentationController.sourceRect = CGRectMake(self.addBtn.frame.size.width/2, 0, 0, 0);
	
	presentationController.sourceView = self.addBtn;
	presentationController.permittedArrowDirections = UIPopoverArrowDirectionDown;
	
	[self presentViewController:controller animated:true completion:nil];
	
	controller.infoLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Adding child node for parent node \"%@\"", @"Adding child node for parent node {Parent node value}"), [(Node*)self.cachedTBView.nodesArray[selectedIndex] value]];
	
}

	//change value of selected node from the cache table
-(IBAction) changeNodeValueBtnClick
{
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ChangeNodeViewController" bundle:nil];
	
	ChangeNodeViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"ChangeNodeViewController"];
	
	if ([self.cachedArray count] == 0)
	{
		UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Changing node",nil) message:NSLocalizedString(@"Download any nodes please before editing",nil) preferredStyle:UIAlertControllerStyleAlert];
		
		UIAlertAction* alertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok",nil) style:UIAlertActionStyleDefault handler:nil];
		[alert addAction:alertAction];
		
		[self presentViewController:alert animated:true completion:nil];
		
		return;
	}
	
	NSInteger selectedIndex = [[self.cachedTBView indexPathForSelectedRow] row];
	Node* selectedNode = self.cachedTBView.nodesArray[selectedIndex];
	
	if (selectedNode.isDeleted)
	{
		[self showAlertNodeIsDeleted];
		return;
	}
	
	controller.nodeToChangeValue = selectedNode;
	
	controller.delegate = self;
	controller.modalPresentationStyle = UIModalPresentationPopover;
	controller.preferredContentSize = CGSizeMake(400, 130);
	
	UIPopoverPresentationController *presentationController = [controller popoverPresentationController];
	
		//move popover arrow to the vertical center of "add" button
	presentationController.sourceRect = CGRectMake(self.changeBtn.frame.size.width/2, 0, 0, 0);
	
	presentationController.sourceView = self.changeBtn;
	presentationController.permittedArrowDirections = UIPopoverArrowDirectionDown;
	
	[self presentViewController:controller animated:true completion:nil];
	
	controller.textFieldValue.text = controller.nodeToChangeValue.value;
	controller.infoLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Change value for node \"%@\"", @"Change value for node {Node value}"), [(Node*)self.cachedTBView.nodesArray[selectedIndex] value]];
}

-(IBAction) deleteNodeBtnClick
{
	if ([self.cachedArray count] == 0)
	{
		UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Deleting node",nil) message:NSLocalizedString(@"Download any nodes please before deleting",nil) preferredStyle:UIAlertControllerStyleAlert];
		
		UIAlertAction* alertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok",nil) style:UIAlertActionStyleDefault handler:nil];
		[alert addAction:alertAction];
		
		[self presentViewController:alert animated:true completion:nil];
		
		return;
	}
	
	NSInteger selectedIndex = [[self.cachedTBView indexPathForSelectedRow] row];
	Node* selectedNode = self.cachedTBView.nodesArray[selectedIndex];
	
	if (selectedNode.isDeleted)
	{
		[self showAlertNodeIsDeleted];
		return;
	}
	
	if ([selectedNode.parentID length] == 0)
	{
		UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Root node",nil) message:NSLocalizedString(@"You cannot remove root node",nil) preferredStyle:UIAlertControllerStyleAlert];
		
		UIAlertAction* alertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok",nil) style:UIAlertActionStyleDefault handler:nil];
		[alert addAction:alertAction];
		
		[self presentViewController:alert animated:true completion:nil];
		return;
	}
	
	[self deleteNode:selectedNode];
	[self.cachedTBView reloadData];
}

#pragma mark nodeOperationsDelegate methods

-(void)addNode:(NSString *)value :(NSString *)parentID
{
	Node* newNode = [[Node alloc] initWithValueAndParentID:value :parentID];
	
	newNode.needToSync = true;
	newNode.syncType = newNode.syncType | syncTypeAdd;
	
	Node* parentNode = [self findNodeWithIDRecursively:parentID :self.cachedArray];
	[parentNode addChild:newNode];
	
	NSUInteger indexParentInTable = [self.cachedTBView.nodesArray indexOfObject:parentNode];
	[self.cachedTBView.nodesArray insertObject:newNode atIndex:indexParentInTable + 1];
	[self.cachedTBView reloadData];
}

-(void)changeNodeValue:(Node *)nodeToChange :(NSString *)newValue
{
	if (newValue != nodeToChange.value)
	{
		nodeToChange.value = newValue;
		nodeToChange.needToSync = true;
		nodeToChange.syncType = nodeToChange.syncType | syncTypeChange;
	
		[self.cachedTBView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[[self cachedTBView]indexPathForSelectedRow]] withRowAnimation:UITableViewRowAnimationNone];
	}
}

-(void)deleteNode:(Node *)nodeToDelete
{
//	if (nodeToDelete.syncType & syncTypeAdd)
//	{
//		if ([nodeToDelete.parentID length] > 0)
//			[[[self findNodeWithIDRecursively:nodeToDelete.parentID :self.cachedArray] children] removeObject:nodeToDelete];
//	
//		[self.cachedTBView.nodesArray removeObject:nodeToDelete];
//	}
	
	nodeToDelete.syncType = nodeToDelete.syncType | syncTypeDelete;
	nodeToDelete.needToSync = true;
	nodeToDelete.isDeleted = true;

	[nodeToDelete deleteNodeChilds];
}

#pragma mark help methods


-(void) refreshCacheTable
{
	[self.cachedTBView getSortedArrayForTableFromArray: self.cachedArray];
	[self.cachedTBView reloadData];
}

-(void) refreshDatabaseTable
{
	self.databaseTBView.nodesArray = [self.database getDataBaseForTable];
	[self.databaseTBView reloadData];
}

-(Node*) findNodeWithIDRecursively:(NSString*)nodeID :(NSMutableArray*) nodes
{
	Node* foundedNode = nil;
	
	for (Node* node in nodes)
	{
		if (foundedNode)
			break;
		
		if ([node.uid isEqualToString:nodeID])
		{
			return node;
		}
		else
		{
			if ([node.children count] > 0)
			{
				foundedNode = [self findNodeWithIDRecursively:nodeID :node.children];
			}
		}
	}
	
	return foundedNode;
}

-(void) showAlertNodeIsDeleted
{
	UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Node is deleted",nil) message:NSLocalizedString(@"You cannot add childs or change value of this node",nil) preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction* alertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok",nil) style:UIAlertActionStyleDefault handler:nil];
	[alert addAction:alertAction];
	
	[self presentViewController:alert animated:true completion:nil];
}


@end
