//
//  ViewController.h
//  test
//
//  Created by IOS developer on 12.08.16.
//  Copyright © 2016 myorg. All rights reserved.
//

@import Foundation;

#import "Database.h"
#import "Tree.h"
#import "Node.h"
#import "CachedTableView.h"
#import "DatabaseTableView.h"
#import "nodeOperationsDelegate.h"

@interface MainViewController : UIViewController <nodeOperationsDelegate>

@property (nonatomic, strong) Database* database;

@property IBOutlet CachedTableView* cachedTBView;
@property IBOutlet DatabaseTableView* databaseTBView;

@property IBOutlet UIButton* addBtn;
@property IBOutlet UIButton* changeBtn;

@property (nonatomic, strong) NSMutableArray* cachedArray;

-(IBAction) applyChangesBtnClick;
-(IBAction) resetAllBtnClick;

-(IBAction) loadNodeBtnClick;
-(IBAction) addNodeBtnClick;
-(IBAction) deleteNodeBtnClick;
-(IBAction) changeNodeValueBtnClick;

@end

typedef enum
{
	syncTypeNone = 0,
	syncTypeAdd = 1 << 0,
	syncTypeChange = 1 << 1,
	syncTypeDelete = 1 << 2
} syncTypes;
